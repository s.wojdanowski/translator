# Jtranslator
Jtranslator jest implementacją translatora gaderypoluki w Javie, Springu i Apache CXF. Zawiera logikę biznesową w formie serwisu oraz kontrolery, za pomocą których można wywołać ten serwis. 
## Technologie
* Java8 lub nowsza
* JUnit 5
* Junit 4
* Spring/SpringBoot
* Apache CXF
## Moduły
 * translator - logika biznesowa
 * rest - moduł obsługi żądań REST
 * ws - moduł obsługi żądań SOAP/WebService
## Uwagi techniczne
### Preferowane IDE
IntelliJ
### Budowanie aplikacji
`mvn clean install`
### Uruchamianie testów
Z wykorzystaniem IDE.
### Uruchamianie aplikacji
Z wykorzystaniem IDE, osobno moduł `rest` oraz `ws`.
### Zapytania REST
Przykładowe zapytanie: `http://localhost:8080/gaderypoluki/napisTestowy`
### Zapytania WS
`curl --header "content-type: text/xml" -d @request.xml http://localhost:8080/services/gaderypoluki`

Można także wykorzystać osobny program **SoapUI**, który zasilamy generowanym plikiem wsdl. Plik można wygenerować żądaniem:
`http://localhost:8080/services/gaderypoluki?wsdl`